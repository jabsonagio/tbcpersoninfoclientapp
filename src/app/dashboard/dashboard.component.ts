import { Component, DebugElement, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppComponent } from '../app.component';
import { DictionariesService } from '../services/dictionaries/dictionaries.service';
import { PersonService } from '../services/person/person.service';
import { ToastrService } from 'ngx-toastr';
import { PersonRequest } from '../person-request.model';
import {NgxPaginationModule} from 'ngx-pagination'; 


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  username:any;
  searchForm: FormGroup;
  addForm: FormGroup;
  relationForm: FormGroup;
  userList;
  showAddForm = false;
  RelationForm: FormGroup;
  RelationTypeList;
  PhoneForm:FormGroup;
  PhoneTypeList;
  GenderList;
  FullData:PersonRequest;
  CurrentUserId:number;
  DetailMode:boolean = false;
  DetailSearchMode: boolean = false;
  p: number = 1;
  collection: any[];


  constructor(private personservice: PersonService,private dictionariesservice: DictionariesService,private appComponent:AppComponent,private fb:FormBuilder,private toastr: ToastrService) {}

  ngOnInit(): void {
    this.username = localStorage.getItem('username');
    this.FullData = new PersonRequest();
    this.initRelationAndPhoneForms();
    this.initDictionaries();
    this.initAddForm();
    this.initSearchForm();
    this.addPhone();
    this.addRelation();
    this.search();
  }


  initDictionaries(){
      this.dictionariesservice.getgenders().subscribe(data => {
        this.GenderList = data;
      },
      error => {
      });

      this.dictionariesservice.getphonetypes().subscribe(data => {
        this.PhoneTypeList = data;
      },
      error => {
      });

      this.dictionariesservice.getrelationtypes().subscribe(data => {
        this.RelationTypeList = data;
      },
      error => {
      });
   }

  initAddForm(){
    this.addForm = new FormGroup({
      FirstName: new FormControl(''),
      LastName: new FormControl(''),
      Gender: new FormControl(0),
      PersonalNumber: new FormControl(''),
      Birthday: new FormControl(''),
    })
  }

  initSearchForm() {
    this.searchForm = new FormGroup({
      FirstName: new FormControl(''),
      LastName: new FormControl(''),
      Gender: new FormControl(0),
      PersonalNumber: new FormControl(''),
      Birthday: new FormControl(''),
    })
  }

  DetailSearchOn(){
    this.clearSearchForm();
    this.DetailSearchMode = true;
    this.search();
  }

  DetailSearchOff(){
    this.clearSearchForm();
    this.DetailSearchMode = false;
    this.search();
  }

  initRelationAndPhoneForms(){
    this.RelationForm = this.fb.group({
      relations: this.fb.array([]) ,
    });

    this.PhoneForm = this.fb.group({
      phones: this.fb.array([]) ,
    });
  }

  search(){
    if (this.searchForm.valid) {
      this.searchForm.value.Birthday = (this.searchForm.value.Birthday == '')?'1900-01-01' : this.searchForm.value.Birthday;

      this.personservice.get(this.searchForm.value).subscribe(data => {
        this.userList = data;
        this.collection = data;
      },
        error => {
          this.toastr.error("could not connect to the server");
        });
    }
  }

  clearSearchForm(){
    this.searchForm.patchValue({
      Birthday: '',
      FirstName:'',
      LastName:'',
      Gender:0,
      PersonalNumber:''
    });
  }

  clearAddForm(){
    this.addForm.patchValue({
      FirstName: '',
      LastName: '',
      Gender: 0,
      PersonalNumber: '',
      Birthday: '',
    });
    
    this.phones.clear();
    this.relations.clear();
    this.addPhone();
    this.addRelation();
  }
  
  fillForm(personId){
    this.personservice.getfullinfo(personId).subscribe(data => {
      this.CurrentUserId = data.person.id;
      this.addForm.patchValue({
        FirstName: data.person.firstName,
        LastName: data.person.lastName,
        Gender: data.person.gender,
        PersonalNumber: data.person.personalNumber,
        Birthday: this.formatDate(data.person.birthday),
      });
    
      this.relations.clear();
      for(let i=0;i<data.relations.length;i++){
        this.addRelation(data.relations[i].id,data.relations[i].relationId,data.relations[i].relationTypeid);
      }

      this.phones.clear();
      for(let i=0;i<data.phones.length;i++){
        this.addPhone(data.phones[i].id,data.phones[i].phoneNumber,data.phones[i].phoneTypeId);
      }

      this.showAddForm = true;
    },
    error => {
      this.toastr.error("could not connect to the server");
    });
  }

  detail(personId){
    this.DetailMode = true;
    this.fillForm(personId);
  }

  addEditPerson(){
    
      this.addForm.value.Birthday = (this.addForm.value.Birthday == '')?'1900-01-01' : this.addForm.value.Birthday;
      this.FullData.Person = this.addForm.value;
      this.FullData.Relations = this.RelationForm.value.relations;
      this.FullData.Phones = this.PhoneForm.value.phones;

      if(this.CurrentUserId == 0){
        this.personservice.add(this.FullData).subscribe(data => {
          if(data.message !=undefined){
            this.toastr.error(data.message);
          }else{
            this.showAddForm = false;
            this.search();
            this.clearAddForm();
            this.toastr.success('Person was successfully added!');
          }
        },
        error => {
          this.toastr.error("could not connect to the server");
        });
      }else{
        this.FullData.Person.id=this.CurrentUserId;
        this.personservice.edit(this.FullData).subscribe(data => {
          if(data.message !=undefined){
            this.toastr.error(data.message);
          }else{
            this.showAddForm = false;
            this.search();
            this.clearAddForm();
            this.toastr.success('Person was successfully updated!');
          }
        },
        error => {
          this.toastr.error("could not connect to the server");
        });
      }


  }

  ShowAddForm(){
    this.showAddForm = true;
    this.CurrentUserId = 0;
  }

  CloseAddForm(){
    this.showAddForm = false;
    this.DetailMode = false;
    this.clearAddForm();
  }

  delete(personId){
   if(confirm('are you sure?')){
     this.personservice.delete(personId).subscribe(data => {
      this.search();
    },
      error => {
        this.appComponent.authorize = false;
      });
   }
  }

  logout(){
    localStorage.clear();
    this.appComponent.authorize = false;
  }

  get relations() : FormArray {
    return this.RelationForm.get("relations") as FormArray
  }

  newRelation(id,relationId,relationTypeId): FormGroup {
    return this.fb.group({
      Id:id,
      RelationId: relationId,
      RelationTypeid: relationTypeId,
    })
  }

  addRelation(id=0,relationId=0,relationTypeId =0) {
    this.relations.push(this.newRelation(id,relationId,relationTypeId));
  }

  removeRelation(i:number) {
    this.relations.removeAt(i);
  }

  get phones() : FormArray {
    return this.PhoneForm.get("phones") as FormArray
  }

  newPhone(id,phoneNumber,phoneTypeId): FormGroup {
    return this.fb.group({
      id:id,
      phoneNumber: phoneNumber,
      phoneTypeId: phoneTypeId,
    })
  }  
  
  addPhone(id=0,phoneNumber='',phoneTypeId=0) {
    this.phones.push(this.newPhone(id,phoneNumber,phoneTypeId));
  }

  removePhone(i:number) {
    this.phones.removeAt(i);
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    return [ year,month, day].join('-');
}


}
