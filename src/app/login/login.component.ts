import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators  } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {AppComponent} from '../app.component'
import { AuthService } from '../services/auth/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  errorResult = false;

  constructor(private authservice: AuthService, private appComponent: AppComponent,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm() {
    this.formGroup = new FormGroup({
      username: new FormControl('tbcuser', [Validators.required]),
      password: new FormControl('tbcuser', [Validators.required])
    })
  }

  login() {
    if (this.formGroup.valid) {
      this.authservice.login(this.formGroup.value).subscribe(data => {
        localStorage.setItem('token', data.token);
        localStorage.setItem('username', data.username);
        this.appComponent.authorize = true;
        this.errorResult = false;
      },
        error => {
          this.toastr.error("could not connect to the server");
        });
    }
  }
}
