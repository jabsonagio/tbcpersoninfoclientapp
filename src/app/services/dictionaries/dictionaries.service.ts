import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DictionariesService {

  constructor(private http:HttpClient) { }

  getphonetypes():Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}Dictionary/getphonetypes`,null,{params:tokenParam});
  }

  getrelationtypes():Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}Dictionary/getrelationtypes`,null,{params:tokenParam});
  }

  getgenders():Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}Dictionary/getgenders`,null,{params:tokenParam});
  }
}
