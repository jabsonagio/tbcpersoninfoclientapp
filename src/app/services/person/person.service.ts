import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private http:HttpClient) { }

  get(data):Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}Person/get`,data,{params:tokenParam});
  }
  
  getfullinfo(personId):Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}Person/getfullinfo?personId=`+personId,null,{params:tokenParam});
  }

  add(data):Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}Person/add`,data,{params:tokenParam});
  }

  edit(data):Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}Person/edit`,data,{params:tokenParam});
  }

  delete(personId):Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}Person/delete/?personId=`+personId,null,{params:tokenParam});
  }
}
